import { component$, useStore, useVisibleTask$ } from '@builder.io/qwik'
import styles from './clock.module.css'

interface SettingsProps {
  settingsObj: {
    milTime: boolean
    showSeconds: boolean
    showAmPm: boolean
  }
}

export const Clock = component$(({ settingsObj }: SettingsProps) => {
  const store = useStore({
    hour: 0,
    milHour: 0,
    minute: 0,
    second: 0,
    datetime: '',
    amPm: '',
  })

  const displayZero = (num: number) => {
    return (num < 10 ? '0' : '') + num
  }

  useVisibleTask$(
    ({ cleanup }) => {
      const getTime = () => {
        const time = new Date()
        store.datetime = time.toISOString()
        store.second = time.getSeconds()
        store.minute = time.getMinutes()
        store.hour = time.getHours() % 12 || 12
        store.milHour = time.getHours()
        store.amPm = time.getHours() >= 12 ? 'PM' : 'AM'
      }
      getTime()
      const timerId = setInterval(getTime, 1000)
      cleanup(() => clearInterval(timerId))
    },
    { strategy: 'document-ready' }
  )

  return (
    <section class={styles.clock}>
      <h2>
        <time dateTime={store.datetime}>
          {settingsObj.milTime ? store.milHour : store.hour}:
          {displayZero(store.minute)}
          {settingsObj.showSeconds && (
            <span class={styles.clock__seconds}>
              :{displayZero(store.second)}
            </span>
          )}
        </time>
        {!settingsObj.milTime && settingsObj.showAmPm && (
          <small class={styles.clock__amPm}>{store.amPm}</small>
        )}
      </h2>
    </section>
  )
})
