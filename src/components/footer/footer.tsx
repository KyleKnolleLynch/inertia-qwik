import { component$, useStylesScoped$ } from '@builder.io/qwik'
import { useGetWallpaper } from '../../routes/layout'
import styles from './footer.css?inline'

export const Footer = component$(() => {
  useStylesScoped$(styles)
  const signal = useGetWallpaper()
  const { value } = signal

  const locationContent = (
    <>
      {!value.location.name &&
      !value.location.city &&
      !value.location.country ? (
        <span>Not provided</span>
      ) : (
        <span>
          {value.location?.name} {value.location?.city}{' '}
          {value.location?.country}
        </span>
      )}
    </>
  )

  const descriptionContent = value.description
    ? value.description
    : value.alt_description
    ? value.alt_description
    : 'No description provided'
 
  return (
    <footer class='footer'>
      <div>
        <p class='footer__description'>{descriptionContent}</p>
        <p class='footer__location'>Location: {locationContent}</p>
      </div>
      <p>
        Photo by:{' '}
        <a href={value.user.links.html} target='_blank'>
          {value.user.name}
        </a>{' '}
        @{' '}
        <a href='https://unsplash.com/' target='_blank'>
          Unsplash
        </a>
      </p>
    </footer>
  )
})
