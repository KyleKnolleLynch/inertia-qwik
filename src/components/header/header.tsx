import {
  $,
  component$,
  useSignal,
  useStore,
  useStylesScoped$,
  useVisibleTask$,
} from '@builder.io/qwik'
import styles from './header.css?inline'

export const Header = component$((props: { showFocus: boolean }) => {
  useStylesScoped$(styles)
  const greeting = useSignal('')

  const greetingObj = useStore({
    name: '',
    focus: '',
  })

  useVisibleTask$(() => {
    const hourNow = new Date().getHours()
    hourNow < 12
      ? (greeting.value = 'Good Morning')
      : hourNow < 18
      ? (greeting.value = 'Good Afternoon')
      : (greeting.value = 'Good Evening')
  })

  useVisibleTask$(() => {
    const prevName = localStorage.getItem('name')
    if (prevName) greetingObj.name = JSON.parse(prevName)

    const prevFocus = localStorage.getItem('focus')
    if (prevFocus) greetingObj.focus = JSON.parse(prevFocus)
  })

  const saveInput = $((input: string, key: string) => {
    const x = JSON.stringify(input)
    localStorage.setItem(`${key}`, x)
  })

  return (
    <header>
      <form preventdefault:submit>
        <h1>
          <span>{greeting.value}</span>
          <label>
            <input
              type='text'
              size={greetingObj.name.length || 14}
              aria-label='Enter your name'
              placeholder='{ Enter Name }'
              value={greetingObj.name}
              onInput$={e => {
                greetingObj.name = (e.target as HTMLInputElement).value
                saveInput(greetingObj.name, 'name')
              }}
              onKeyDown$={e =>
                e.key === 'Enter' && (e.target as HTMLInputElement).blur()
              }
            />
          </label>
        </h1>

        {props.showFocus && (
          <h2>
            <label>
              Daily Focus
              <input
                type='text'
                aria-label='Daily focus goal'
                placeholder=' '
                value={greetingObj.focus}
                onInput$={e => {
                  greetingObj.focus = (e.target as HTMLInputElement).value
                  saveInput(greetingObj.focus, 'focus')
                }}
                onKeyDown$={e =>
                  e.key === 'Enter' && (e.target as HTMLInputElement).blur()
                }
              />
            </label>
          </h2>
        )}

        <input type='submit' class='hidden' />
      </form>
    </header>
  )
})
