import { $, component$, useSignal, useStylesScoped$ } from '@builder.io/qwik'

import styles from './settings.css?inline'

interface SettingsProps {
  settingsObj: {
    milTime: boolean
    showSeconds: boolean
    showAmPm: boolean
    showFocus: boolean
  }
}

export const Settings = component$(({ settingsObj }: SettingsProps) => {
  useStylesScoped$(styles)
  const showPanel = useSignal(false)

  const saveInput = $((input: boolean, key: string) => {
    const x = JSON.stringify(input)
    localStorage.setItem(`${key}`, x)
  })

  return (
    <section class='settingsSection'>
      <fieldset
        class={`settingsSection__panel ${showPanel.value && 'show-panel'}`}
      >
        <legend>Settings Panel</legend>
        <div>
          <input
            type='checkbox'
            id='milTime'
            name='milTime'
            onClick$={() => {
              settingsObj.milTime = !settingsObj.milTime
              saveInput(settingsObj.milTime, 'milTime')
            }}
            checked={settingsObj.milTime}
          />
          <label for='milTime'>24hr Format</label>
        </div>

        <div>
          <input
            type='checkbox'
            id='showAmPm'
            name='showAmPm'
            onClick$={() => {
              settingsObj.showAmPm = !settingsObj.showAmPm
              saveInput(settingsObj.showAmPm, 'showAmPm')
            }}
            checked={settingsObj.showAmPm}
          />
          <label for='showAmPm'>Show Am/Pm</label>
        </div>

        <div>
          <input
            type='checkbox'
            id='showSeconds'
            name='showSeconds'
            onClick$={() => {
              settingsObj.showSeconds = !settingsObj.showSeconds
              saveInput(settingsObj.showSeconds, 'showSeconds')
            }}
            checked={settingsObj.showSeconds}
          />
          <label for='showSeconds'>Show Seconds</label>
        </div>

        <div>
          <input
            type='checkbox'
            id='showFocus'
            name='showFocus'
            onClick$={() => {
              settingsObj.showFocus = !settingsObj.showFocus
              saveInput(settingsObj.showFocus, 'showFocus')
            }}
            checked={settingsObj.showFocus}
          />
          <label for='showFocus'>Show Focus</label>
        </div>
      </fieldset>

      <button
        class='settingsSection__cog'
        aria-label='Toggle settings panel'
        onClick$={() => (showPanel.value = !showPanel.value)}
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          stroke-width='2'
          stroke-linecap='round'
          stroke-linejoin='round'
          class={showPanel.value ? 'rotate-cog' : ''}
        >
          <circle cx='12' cy='12' r='3'></circle>
          <path d='M19.4 15a1.65 1.65 0 0 0 .33 1.82l.06.06a2 2 0 0 1 0 2.83 2 2 0 0 1-2.83 0l-.06-.06a1.65 1.65 0 0 0-1.82-.33 1.65 1.65 0 0 0-1 1.51V21a2 2 0 0 1-2 2 2 2 0 0 1-2-2v-.09A1.65 1.65 0 0 0 9 19.4a1.65 1.65 0 0 0-1.82.33l-.06.06a2 2 0 0 1-2.83 0 2 2 0 0 1 0-2.83l.06-.06a1.65 1.65 0 0 0 .33-1.82 1.65 1.65 0 0 0-1.51-1H3a2 2 0 0 1-2-2 2 2 0 0 1 2-2h.09A1.65 1.65 0 0 0 4.6 9a1.65 1.65 0 0 0-.33-1.82l-.06-.06a2 2 0 0 1 0-2.83 2 2 0 0 1 2.83 0l.06.06a1.65 1.65 0 0 0 1.82.33H9a1.65 1.65 0 0 0 1-1.51V3a2 2 0 0 1 2-2 2 2 0 0 1 2 2v.09a1.65 1.65 0 0 0 1 1.51 1.65 1.65 0 0 0 1.82-.33l.06-.06a2 2 0 0 1 2.83 0 2 2 0 0 1 0 2.83l-.06.06a1.65 1.65 0 0 0-.33 1.82V9a1.65 1.65 0 0 0 1.51 1H21a2 2 0 0 1 2 2 2 2 0 0 1-2 2h-.09a1.65 1.65 0 0 0-1.51 1z'></path>
        </svg>
      </button>
    </section>
  )
})
