import {
  component$,
  useStylesScoped$,
  useSignal,
  $,
} from '@builder.io/qwik'

import styles from './todos.css?inline'

// interface ListItem {
//   text: string
// }

// export const list: ListItem[] = []

// export const useListLoader = routeLoader$(() => {
//   return list;
// });

// export const useAddToListAction = globalAction$(
//   item => {
//     list.push(item)
//     return {
//       success: true,
//     }
//   },
//   zod$({
//     text: z.string(),
//   })
// )

export const Todos = component$(() => {
  useStylesScoped$(styles)
  const showPanel = useSignal(false)
  const text = useSignal('')

const list: any[] = []

  // const useAddToListAction = globalAction$(
  //   item => {
  //     list.push(item)
  //     return {
  //       success: true,
  //     }
  //   },
  //   zod$({
  //     text: z.string(),
  //   })
  // )

  const onFormSubmit = $(() => {
    list.push(text.value)
  
    console.log(list.map(item => {return item}))
  })

  return (
    <section class='todoListSection'>
      <div class={`todoListSection__panel ${showPanel.value && 'show-panel'}`}>
        <form
          class='todoListSection__form'
          onSubmit$={onFormSubmit}
          preventdefault:submit
        >
          <label>
            <input type='text' name='text' value={text.value} required onInput$={(e) => text.value = (e.target as HTMLInputElement).value} />
          </label>
          <button type='submit'>Add Todo</button>
        </form>
        {list.length && (
          <ul>
            {list.map((item) => (
              <li key={`items-${Math.random() * Math.random()}`}>{item}</li>
            ))}
          </ul>
        ) || (
          <p>No Todos Found</p>
        )}
      </div>

      <button
        type='button'
        aria-label='Toggle todo list'
        class='todoListSection__toggle'
        onClick$={() => (showPanel.value = !showPanel.value)}
      >
        <svg
          xmlns='http://www.w3.org/2000/svg'
          width='28'
          height='28'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          stroke-width='2'
          stroke-linecap='round'
          stroke-linejoin='round'
          class='feather feather-list'
        >
          <line x1='8' y1='6' x2='21' y2='6'></line>
          <line x1='8' y1='12' x2='21' y2='12'></line>
          <line x1='8' y1='18' x2='21' y2='18'></line>
          <line x1='3' y1='6' x2='3.01' y2='6'></line>
          <line x1='3' y1='12' x2='3.01' y2='12'></line>
          <line x1='3' y1='18' x2='3.01' y2='18'></line>
        </svg>
      </button>
    </section>
  )
})
