import { component$, useStore, useVisibleTask$ } from '@builder.io/qwik'
import type { DocumentHead } from '@builder.io/qwik-city'

// components
import { Header } from '~/components/header/header'
import { Clock } from '~/components/clock/clock'
// import { Todos } from '~/components/todos/todos'
import { Settings } from '~/components/settings/settings'
import { Footer } from '~/components/footer/footer'

export default component$(() => {
  const settingsObj = useStore({
    milTime: false,
    showSeconds: false,
    showAmPm: false,
    showFocus: false,
  })

  useVisibleTask$(() => {
    const milTime = localStorage.getItem('milTime')
    if (milTime) settingsObj.milTime = JSON.parse(milTime)

    const showAmPm = localStorage.getItem('showAmPm')
    if (showAmPm) settingsObj.showAmPm = JSON.parse(showAmPm)
    if (showAmPm === null) settingsObj.showAmPm = true

    const showSeconds = localStorage.getItem('showSeconds')
    if (showSeconds) settingsObj.showSeconds = JSON.parse(showSeconds)

    const showFocus = localStorage.getItem('showFocus')
    if (showFocus) settingsObj.showFocus = JSON.parse(showFocus)
    if (showFocus === null) settingsObj.showFocus = true
  })

  return (
    <>
      <Header showFocus={settingsObj.showFocus} />

      <Clock settingsObj={settingsObj} />

      {/* <Todos /> */}

      <Settings settingsObj={settingsObj} />

      <Footer />
    </>
  )
})

export const head: DocumentHead = {
  title: 'Inertia',
  meta: [
    {
      name: 'description',
      content: 'A dynamic welcome page application',
    },
  ],
}
