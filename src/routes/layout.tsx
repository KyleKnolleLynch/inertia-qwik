import { component$, Slot, useTask$, useSignal } from '@builder.io/qwik'
import { routeLoader$ } from '@builder.io/qwik-city'
import { isServer } from '@builder.io/qwik/build'

// export const useServerTimeLoader = routeLoader$(() => {
//   return {
//     date: new Date().toISOString(),
//   }
// })

//  Get current time/hours for dynamic image query
const setQuery = () => {
  const hourNow = new Date().getHours()
  let dynamicQuery

  if (hourNow < 4) {
    dynamicQuery = 'night'
  } else if (hourNow < 12) {
    dynamicQuery = 'nature+morning'
  } else if (hourNow < 18) {
    dynamicQuery = 'nature+afternoon'
  } else if (hourNow < 20) {
    dynamicQuery = 'nature+evening'
  } else {
    dynamicQuery = 'night'
  }
  return dynamicQuery
}

const dynamicQuery = setQuery()

const BASE_URL = 'https://api.unsplash.com/photos/random'
const UNSPLASH_CLIENT_ID = process.env.UNSPLASH_KEY
const API_URL = `${BASE_URL}?client_id=${UNSPLASH_CLIENT_ID}&query=${dynamicQuery}&auto=format`

export const useGetWallpaper = routeLoader$(() => {
  const fetchUnsplashApi = async () => {
    const res = await fetch(API_URL)
    const data = await res.json()

    return data
  }
  return fetchUnsplashApi()
})

export default component$(() => {
  const signal = useGetWallpaper()
  const { value } = signal

  // Get window height to use as picture element attribute
  const height = useSignal(0)
  useTask$(({ track }) => {
    track(() => height.value)
    if (isServer) {
      return
    }
    isNaN(window.innerHeight)
      ? (height.value = document.documentElement.clientHeight)
      : (height.value = window.innerHeight)
  })

  return (
    <div class='app__container'>
      <div class='layout-wallpaper__container'>
        <picture>
          <source
            srcSet={value.urls.regular}
            media='(min-width: 600px)'
            width='1080'
            height={height.value}
          />
          <img
            src={value.urls.small}
            alt={value.alt_description || value.description || 'Unsplash.com'}
            width='400'
            height={height.value}
            class='layout-wallpaper__img'
          />
        </picture>
      </div>
      <main class='content-container'>
        <Slot />
      </main>
    </div>
  )
})
